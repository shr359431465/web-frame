var Validate = {
    createValidate: function(){
        var validate = {};
        var valArr = ["phone","email","required","password"];
        var messageMap = new Map();
        messageMap.set("phone","电话格式不正确！");
        messageMap.set("email","邮箱格式不正确！");
        messageMap.set("required","不能为空！");
        messageMap.set("password","密码格式不正确！");

        validate.checkSimple = function(checkType,checkId,errorClass,Message){
            if($.inArray(checkType, valArr) >= 0){
                var checkValue = $("#"+checkId).val();
                var checkStatus;
                switch (checkType){
                    case "phone":
                        checkStatus = this.checkPhoneValidate(checkValue);
                        break;
                    case "email":
                        checkStatus = this.checkEmailValidate(checkValue);
                        break;
                    case "required":
                        checkStatus = this.checkRequiredValidate(checkValue);
                        break;
                    case "password":
                        checkStatus = this.checkPasswordValidate(checkValue);
                        break;
                }

                if(checkStatus){
                    return true;
                }else{
                    if(Message == undefined){
                        Message = messageMap.get(checkType);
                    }
                    $("."+errorClass).append("<div style='color:#ff4500;'>"+Message+"</div>");
                    return false;
                }
            }
        };

        validate.checkPhoneValidate = function (checkValue) {
            var regexp = /^[1][3,4,5,7,8][0-9]{9}$/;
            if(!regexp.test(checkValue)){
                return false;
            }else{
                return true;
            }
        };

        validate.checkEmailValidate = function (checkValue) {
            var regexp = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
            if(!regexp.test(checkValue)){
                return false;
            }else{
                return true;
            }
        };

        validate.checkRequiredValidate = function (checkValue) {
            if(!checkValue){
                return false;
            }else{
                return true;
            }
        };

        validate.checkPasswordValidate = function (checkValue) {
            if(!checkValue){
                return false;
            }else{
                if(checkValue.length > 8 && checkValue.length < 18){
                    return true;
                }else{
                    return false;
                }
            }
        };

        return validate;
    }
};