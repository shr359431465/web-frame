$(function(){
	var account = "";
	$(document).keydown(function(event){
		if(event.keyCode == "13"){
			$("#login").click();
		}
	});


	$("#login").click(function(){
		account = $("#signin-account").val();
		var password = $("#signin-password").val();
		var ownerId = $("#owner-id").val();
		var type = "0";

		if(account == "" || password == ""){
			tip("用户名或密码不能为空！","error");
			return;
		}else{
			var data = JSON.stringify({"data": {"account":account,"password": password,"type": type,"ownerId":ownerId}});
			var url = "/User/UserLogin";
			ajaxJsonRequest(url,data,login);
		}
	});	

	function login(result){
	    var storage=window.sessionStorage;
		var user = JSON.stringify(result['data']['user']);
		storage.setItem("user",user);
		storage.setItem("signAccount",account);
        window.location="./template/index.html";
	}
})