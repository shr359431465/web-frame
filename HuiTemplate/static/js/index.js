$(function () {
    //密码修改
    $(".self-close").click(function () {
        $(".edit-modal-self-password").modal("hide");
    });

    $(".self-pass-edit").click(function () {
        var selfAccountIdPass = $("input[name='self_account_id_pass']").val();
        var selfPasswordUpdate = $("input[name='self_password_update']").val();
        if(selfPasswordUpdate.length < 8 && selfPasswordUpdate.length > 18){
            return false;
        }

        var url = "/sys/base/user/password/update";
        var data = JSON.stringify({"data":{"accountId":selfAccountIdPass,"password":selfPasswordUpdate}});
        ajaxJsonRequest(url,data,function (obj) {
            $("input[name='self_password_update']").val("");
            $(".edit-modal-self-password").modal("hide");
        });
    });
})