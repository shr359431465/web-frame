﻿﻿var username;
var site;
var userId;
var signAccount;
var permission;
var ipPath = "http://192.168.131.22:8080";
var websocketIp = "ws://192.168.131.22:8080";
// var ipPath = "";
// var websocketIp = "wss://" + window.location.host;
var message = "服务异常或长时间未操作，请重新登陆";

function notice() {
    var data = JSON.stringify({"data":{}});
    var url = "/transports/getAllCars";
    ajaxJsonRequest(url,data,function () {});
}

function getUserInfo() {
    var storage = window.sessionStorage;
    var user = storage.getItem("user");
    var userObj = JSON.parse(user);
    username = userObj['username'];
    site = userObj['site'];
    userId = userObj['accountId'];
    signAccount = storage.getItem("signAccount");
    permission = userObj['permission'];
    $("#username").text(username);
    $("input[name='self_account_update']").val(signAccount);
    $("input[name='self_username_update']").val(username);
    $("input[name='self_account_id_pass']").val(userId);
}

//当前是否登录验证
function logincheck() {
	var storage = window.sessionStorage;
	var user = storage.getItem("user");
	if(user != {} && user != null){
        window.location="../../template/index.html";
	}
}

//退出
function layout() {
	var data = JSON.stringify({"data":{}});
	var url = "/User/logOut";
	ajaxJsonRequest(url,data,function () {
        var storage=window.sessionStorage;
        storage.clear();
        window.location="../login.html";
    });
}


//验证登录状态
function validate() {
	var storage = window.sessionStorage;
	var user = storage.getItem("user");
	if(user == {} || user == null){
		window.location="./404.html";
	}
}

//弹窗提示
function tipBox(mess){
	$(".tip-box").find("p").text(mess);
	$(".tip-box").removeClass("hid");
	setTimeout(function(){
		$(".tip-box").addClass("hid");
	},4000);
}

/*
** 失败，错误提示框
**
*/
function tip(mess,tip){
	$(".tipcontent").text(mess);
	$("#tip").removeClass("alert-danger");
	$("#tip").removeClass("alert-success");
	if(tip == "success"){
		$("#tip").addClass("alert-success");
	}else if(tip == "error"){
		$("#tip").addClass("alert-danger");
	}
	$("#tip").show();
	setTimeout(function(){
		$('#tip').hide();
		$(".tipcontent").text("");
	},4000);
}

function isInArray2(arr,value){
	var index = $.inArray(value,arr);
	if(index >= 0){
		return true;
	}
	return false;
}


function checkValue(val){
	if(val == undefined || val == "NULL"){
		val = "";
		return val;
	}
	return val;
}

function checkDoubleValue(val, fixLen){
	if(val == undefined){
		val = "";
		return val;
	}
	if (fixLen != null && fixLen >= 0) {
	    var num = Number(val);
	    if (!isNaN(num)) {
	        return num.toFixed(fixLen);
	    }
	}
	return val;
}

function checkUnValue(val) {
	if(val == undefined){
		val = "　";
		return val;
	}
	return val;
}

//左侧导航权限
function sliderInfo(){
	var arrayPer = new Array();
	var sliderHtml = '';
	arrayPer = permission.split(",");
	
	if(isInArray2(arrayPer,"1")){
		sliderHtml += '<dl id="menu-article">\n' +
            '\t\t\t\t<dt><i class="Hui-iconfont">&#xe616;</i> 模板<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>\n' +
            '\t\t\t\t<dd>\n' +
            '\t\t\t\t\t<ul>\n' +
            '\t\t\t\t\t\t<li><a data-href="style.html" data-title="模板" href="javascript:void(0)">模板</a></li>\n' +
            '\t\t\t\t\t</ul>\n' +
            '\t\t\t\t</dd>\n' +
            '\t\t\t</dl>';
	}
	$("#sliderlist").html(sliderHtml);
}

function ajaxJsonRequest(url,data,callBack,params){
    $.ajax({
        type: "POST",
        url: ipPath+url,
        contentType: "application/json; charset=utf-8",
        data:data,
        xhrFields:{withCredentials:true},
        crossDomain:true,
        success:function(result){
        	if(result['code'] == 200){
                callBack(result,params);
			}else{
                tipBox(result["message"]);
			}
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
           tipBox(message);
        }
    });
}


function ajaxKYRequest(url,data,callBack,params){
    $.ajax({
        type: "POST",
        url: ipPath+url,
        data:data,
        xhrFields:{withCredentials:true},
        crossDomain:true,
        success:function(result){
            if(result['code'] == 200){
                callBack(result,params);
            }else{
                tipBox(result["message"]);
            }
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
        	tipBox();
        }
    });
}